package com.telerikproject.controllers;

import com.telerikproject.models.Comment;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import com.telerikproject.services.interfaces.CommentService;
import com.telerikproject.services.interfaces.PostService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private UserService userService;

    private PostService postService;

    private CommentService commentService;

    @Autowired
    public AdminController(UserService userService, PostService postService, CommentService commentService) {
        this.userService = userService;
        this.postService = postService;
        this.commentService = commentService;
    }

    @GetMapping(path = "/edit/{id}")
    public String getEditUserView(Model model, @PathVariable("id") int id) {
        User user = userService.getUserById(id);
        model.addAttribute("user", user);
        model.addAttribute("userDetails", user.getUserDetails());
        return "edit-user";
    }

    @PutMapping("/edit/{id}")
    public String editUser(@ModelAttribute User user,
                           @PathVariable int id) {
        User userToBeUpdated = userService.getUserById(id);

        userToBeUpdated.setFirstName(user.getFirstName());
        userToBeUpdated.setLastName(user.getLastName());
        userToBeUpdated.setLastName(user.getLastName());

        userService.save(id, userToBeUpdated);

        return "redirect:/admin";
    }

    @RequestMapping(path = "/delete/user/{username}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteUser(@PathVariable("username") String username) {
        userService.deleteUser(username);
        return "redirect:/admin";
    }

    @GetMapping(path = "/edit/post/{id}")
    public String getEditPostView(Model model, @PathVariable("id") int id) {
        Post post = postService.getPostById(id);
        model.addAttribute("post", post);
        return "edit-post";
    }

    @PutMapping("/edit/post/{id}")
    public String editPost(@ModelAttribute Post post, @PathVariable int id) {
        postService.editPost(id, post);
        return "redirect:/admin";
    }

    @RequestMapping(path = "/delete/post/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deletePostById(@PathVariable("id") int id) {
        postService.deletePost(id);
        return "redirect:/";
    }

    @GetMapping(path = "/edit/comment/{id}")
    public String getEditCommentView(Model model, @PathVariable("id") int id) {
        Comment comment = commentService.getCommentById(id);
        model.addAttribute("comment", comment);
        return "edit-comment";
    }

    @PutMapping("/edit/comment/{id}")
    public String editComment(@ModelAttribute Comment comment, @PathVariable int id) {
        commentService.editComment(id, comment);
        return "redirect:/admin";
    }

    @RequestMapping(path = "/delete/comment/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteComment(@PathVariable("id") int id) {
        try {
            commentService.deleteComment(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "redirect:/admin";
    }
}
