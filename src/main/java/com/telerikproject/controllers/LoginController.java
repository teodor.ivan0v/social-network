package com.telerikproject.controllers;

import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class LoginController {

    private UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }

    @GetMapping("/profile")
    public String showProfile(Model model, Principal principal) {
        User currentUser = userService.getCurrentUser(principal);
        model.addAttribute("user", currentUser);
        model.addAttribute("userDetails", currentUser.getUserDetails());
        model.addAttribute("post", new Post());
        return "profile";
    }

    @GetMapping("/admin")
    public String showAdminLoginPage() {
        return "admin";
    }
}
