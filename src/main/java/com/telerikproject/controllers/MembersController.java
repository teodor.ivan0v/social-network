package com.telerikproject.controllers;

import com.telerikproject.models.User;
import com.telerikproject.services.interfaces.ConnectionService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

@Controller
public class MembersController {

    private UserService userService;

    @Autowired
    public MembersController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/members")
    public String getAllUsersView() {
        return "members";
    }
}
