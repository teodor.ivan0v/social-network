package com.telerikproject.controllers;

import com.telerikproject.DTOs.PostDTO;
import com.telerikproject.models.Post;
import com.telerikproject.services.interfaces.PostService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/posts")
public class PostController {

    private static final String PROFILE = "profile";
    private PostService postService;
    private UserService userService;

    @Autowired
    public PostController(PostService postService, UserService userService) {
        this.postService = postService;
        this.userService = userService;
    }

    @GetMapping
    public String getPostsView() {
        return "index";
    }

    @GetMapping("/")
    public String getCreatePostView(Model model){
        model.addAttribute("post", new Post());
        return "index";
    }

    @PostMapping("/")
    @RequestMapping
    public String createPost(@Valid @ModelAttribute PostDTO postDTO,
                             @RequestParam(name = "page", required=false) String page,
                             BindingResult bindingResult,
                             Model model,
                             Principal principal    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Post can't be empty!");
            return "index";
        }

        postService.createPost(postDTO, userService.getCurrentUser(principal));

        if (PROFILE.equals(page)){
            return "redirect:/profile";
        }

        return "redirect:/posts";
    }
}
