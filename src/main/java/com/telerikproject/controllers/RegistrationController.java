package com.telerikproject.controllers;

import com.telerikproject.models.User;
import com.telerikproject.models.UserDetails;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UserService userService;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @GetMapping("/register-confirmation")
    public String registerConfirmation() {
        return "login";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) {

        String errorMessage = validateUser(user, bindingResult);

        if (!errorMessage.isEmpty()){
            model.addAttribute("error", errorMessage);
            return "register";
        }
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");

        org.springframework.security.core.userdetails.User newUser =  new org.springframework.security.core.userdetails.User(user.getUsername(),
                passwordEncoder.encode(user.getPassword()), authorities);

        userDetailsManager.createUser(newUser);

        User customUser = userService.getUserByUsername(newUser.getUsername());
        customUser.setFirstName(user.getFirstName());
        customUser.setLastName(user.getLastName());
        customUser.setGender(user.getGender());
        customUser.setUserDetails(new UserDetails());

        if(customUser.getGender().equals("Male")) {
            customUser.getUserDetails().setImagePath("user.png");
        } else if (customUser.getGender().equals("Female")) {
            customUser.getUserDetails().setImagePath("user_woman.png");
        } else {
            throw new IllegalArgumentException("Enter sex");
        }
        customUser.getUserDetails().setCountry("Not specified");
        customUser.getUserDetails().setCity("Not specified");
        customUser.getUserDetails().setAbout("Not specified");

        try {
            userService.createUser(customUser);
        } catch (IOException e) {
            System.out.println("Caught IOException: " + e.getMessage());
        }

        return "login";

    }

    private String validateUser(@ModelAttribute @Valid User user, BindingResult bindingResult) {
        if (user.getGender() == null) {
            return "Sex can't be empty!";
        }

        if (bindingResult.hasErrors()) {
            return "Username/password can't be empty!";
        }

        if(userDetailsManager.userExists(user.getUsername())) {
            return "User with same email already exists!";
        }

        if(!user.getPassword().equals(user.getMatchingPassword())) {
            return "Password doesn't match!";
        }

        return "";
    }

}
