package com.telerikproject.controllers;

import com.telerikproject.models.User;
import com.telerikproject.services.interfaces.ConnectionService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;

@Controller
@RequestMapping("/user")
public class UserController {

    private UserService userService;
    private ConnectionService connectionService;

    private static String uploadDirectory = System.getProperty("user.dir") + "/src/main/resources/static/img";

    @Autowired
    public UserController(UserService userService, ConnectionService connectionService) {
        this.userService = userService;
        this.connectionService = connectionService;
    }

    @RequestMapping("/{id}")
    public String getUserProfileById(@PathVariable int id, Model model) {
        User user = userService.getUserById(id);
        model.addAttribute("user", user);
        return "user-profile";
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.PUT)
    public String editUser(@ModelAttribute User user,
                           Principal principal,
                           @PathVariable int id) {
        userService.save(id, user);

        return "redirect:/profile";
    }

    @RequestMapping("/profile")
    public String UploadPage() {
        return "profile";
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("files") MultipartFile[] files, Principal principal) {
        StringBuilder fileNames = new StringBuilder();
        for (MultipartFile file : files) {
            Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
            fileNames.append(file.getOriginalFilename());
            try {
                Files.write(fileNameAndPath, file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        User user = userService.getUserByUsername(principal.getName());

        user.getUserDetails().setImagePath(fileNames.toString());

        userService.save(user);

        return "redirect:/profile";
    }

    @GetMapping("/user/decline/{username}")
    public String declinePersonFriendRequest(@PathVariable String username, Principal principal) {
        connectionService.declineFriendRequest(username, userService.getCurrentUser(principal));
        return "redirect:/profile";
    }
}
