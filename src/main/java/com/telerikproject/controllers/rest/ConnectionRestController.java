package com.telerikproject.controllers.rest;

import com.telerikproject.exceptions.ExistingException;
import com.telerikproject.models.Connection;
import com.telerikproject.models.User;
import com.telerikproject.services.interfaces.ConnectionService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.List;

@RestController()
@RequestMapping("/api/connection")
public class ConnectionRestController {

    private ConnectionService connectionService;
    private UserService userService;

    @Autowired
    public ConnectionRestController(ConnectionService connectionService, UserService userService) {
        this.connectionService = connectionService;
        this.userService = userService;
    }

    @GetMapping("/getSendFriendRequests")
    public List<Connection> getSendFriendRequests(Principal principal) {
        return connectionService.getAllSendRequests(userService.getCurrentUser(principal));
    }

    @GetMapping("/getFriendRequests")
    public List<User> getFriendRequests() {
        List<User> users;
        try {
            users = connectionService.getAllRequestsFromUsers();
            return users;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/currentUserFriends")
    public List<User> getFriends(Principal principal) {
        List<User> users;
        try {
            users = connectionService.getAllFriends(userService.getCurrentUser(principal));
            return users;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/accept/{id}")
    public User acceptRequest(@PathVariable int id, Principal principal) {
        try {
            return connectionService.acceptFriendRequest(userService.getUserById(id).getUsername(), userService.getCurrentUser(principal));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/decline/{id}")
    public void declineRequest(@PathVariable int id, Principal principal) {
        try {
            connectionService.declineFriendRequest(userService.getUserById(id).getUsername(), userService.getCurrentUser(principal));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteRequest(@PathVariable int id, Principal principal) {
        try {
            connectionService.deleteFriendRequest(userService.getUserById(id).getUsername(), userService.getCurrentUser(principal));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @RequestMapping(value = "/send-request/{id}", method = RequestMethod.POST)
    public String sendPersonFriendRequest(@PathVariable int id, Principal principal) {
        User user = userService.getUserById(id);
        try {
            connectionService.sendFriendRequest(user.getUsername(), userService.getCurrentUser(principal));
        } catch (ExistingException e) {
            System.out.println(e.getMessage());
        }

        return "redirect:/profile";
    }
}