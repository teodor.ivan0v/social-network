package com.telerikproject.controllers.rest;

import com.telerikproject.models.Like;
import com.telerikproject.services.interfaces.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/likes")
public class LikeRestController {

    private LikeService likeService;

    @Autowired
    public LikeRestController(LikeService likeService) {
        this.likeService = likeService;
    }

    @GetMapping("/post/{id}")
    public List<Like> getAllLikesByPostId(@PathVariable int id) {
        return likeService.getAllLikesByPostId(id);
    }
}
