package com.telerikproject.controllers.rest;

import com.telerikproject.exceptions.PostDoesNotExistException;
import com.telerikproject.models.Like;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import com.telerikproject.services.interfaces.CommentService;
import com.telerikproject.services.interfaces.LikeService;
import com.telerikproject.services.interfaces.PostService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostRestController {

    private PostService postService;
    private LikeService likeService;
    private UserService userService;

    @Autowired
    public PostRestController(PostService postService, LikeService likeService, UserService userService) {
        this.postService = postService;
        this.likeService = likeService;
        this.userService = userService;
    }

    @GetMapping("/user/{id}")
    public List<Post> getPostsByUserId(@PathVariable int id) {
        return userService.getUserById(id).getPosts();
    }

    @GetMapping("/all")
    public List<Post> getAllPosts() {
        try {
            return postService.getAll();
        } catch (PostDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/allPublic")
    public List<Post> getAllPublicPostsOrderByPostDateDesc() {
        try {
            return postService.getAllPublicPostsOrderByPostDateDesc();
        } catch (PostDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public Post getPostById(@PathVariable int id) {
        try {
            return postService.getPostById(id);
        } catch (PostDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/like")
    public void likeAndDislikePost(@PathVariable int id, Principal principal) {
        try {
            likeService.likeAndDislikePost(id, userService.getCurrentUser(principal));
        } catch (NotImplementedException e) {
            System.out.println("asd");
        }
    }

    @GetMapping("/profile/all")
    public List<Post> getAllCurrentUserPosts(Principal principal) {
        return postService.getAllCurrentUserPosts(userService.getCurrentUser(principal));
    }

    @Transactional
    @DeleteMapping("/{id}")
    public void deletePost(@PathVariable Integer id) {
        try {
            postService.deletePost(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
