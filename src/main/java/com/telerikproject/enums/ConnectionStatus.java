package com.telerikproject.enums;

public enum ConnectionStatus {
    PENDING,
    ACCEPTED,
    DECLINED
}
