package com.telerikproject.exceptions;

public class CommentDoesNotExistException extends RuntimeException {
    public CommentDoesNotExistException() {
        super();
    }
}
