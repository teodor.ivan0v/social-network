package com.telerikproject.exceptions;

public class ConnectionDoesNotExistException extends RuntimeException {
    public ConnectionDoesNotExistException() {
        super();
    }
}
