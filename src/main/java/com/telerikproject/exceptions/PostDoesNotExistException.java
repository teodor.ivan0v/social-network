package com.telerikproject.exceptions;

public class PostDoesNotExistException extends RuntimeException {
    public PostDoesNotExistException() {
        super();
    }
}
