package com.telerikproject.models;

import com.telerikproject.enums.ConnectionStatus;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "connections")
public class Connection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "first_user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User firstUser;

    @ManyToOne
    @JoinColumn(name = "second_user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User secondUser;

    private ConnectionStatus status;

    public Connection() {
    }

    public Connection(User firstUser, User secondUser, ConnectionStatus status) {
        this.firstUser = firstUser;
        this.secondUser = secondUser;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getFirstUser() {
        return firstUser;
    }

    public void setFirstUser(User firstUser) {
        this.firstUser = firstUser;
    }

    public User getSecondUser() {
        return secondUser;
    }

    public void setSecondUser(User secondUser) {
        this.secondUser = secondUser;
    }

    public ConnectionStatus getStatus() {
        return status;
    }

    public void setStatus(ConnectionStatus status) {
        this.status = status;
    }
}
