package com.telerikproject.repositories;

import com.telerikproject.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;


public interface CommentRepository extends JpaRepository<Comment, Integer> {

    @Modifying
    @Transactional
    @Query(value="delete from Comment c where c.post.id = :postId")
    void deleteAllCommentsByPostId(Integer postId);

    @Modifying
    @Transactional
    @Query(value="delete from Comment c where c.id = :id")
    void deleteAllById(Integer id);
}
