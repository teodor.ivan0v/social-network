package com.telerikproject.repositories;

import com.telerikproject.enums.ConnectionStatus;
import com.telerikproject.models.Connection;
import com.telerikproject.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ConnectionRepository extends JpaRepository<Connection, Integer> {

    Connection findByFirstUserAndSecondUser(User firstUser, User secondUser);

    Connection findByFirstUserAndSecondUserAndStatus(User firstUser, User secondUser, ConnectionStatus status);

    List<Connection> getAllBySecondUserAndStatus(User user,ConnectionStatus status);

    List<Connection> getAllByFirstUserAndStatus(User user, ConnectionStatus status);

    List<Connection> getAllByFirstUser(User user);

}
