package com.telerikproject.repositories;

import com.telerikproject.models.Like;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface LikeRepository extends JpaRepository<Like, Integer> {

    List<Like> getAllByIsLikedTrue();

    List<Like> getAllByPostId(Integer id);

    Like getLikeByIdAndIsLikedTrue(Integer id);

    Optional<Like> getOptionalByUserAndPost(User user, Post post);

    Like getLikeByUserAndPost(User user, Post post);
}
