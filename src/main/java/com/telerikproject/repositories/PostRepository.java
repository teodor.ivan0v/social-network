package com.telerikproject.repositories;

import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import javafx.geometry.Pos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

        @Query("select p from Post p where is_public = 0 order by post_date desc")
        List<Post> getAllPublicPostsOrderByPostDateDesc();

        @Query("select p from Post p order by post_date desc")
        List<Post> getAllPostsOrderByPostDateDesc();

        List<Post> getAllByUserId(Integer id);

        void deletePostById(int id);
}
