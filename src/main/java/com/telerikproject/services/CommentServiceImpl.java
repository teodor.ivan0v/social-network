package com.telerikproject.services;

import com.telerikproject.DTOs.CommentDTO;
import com.telerikproject.exceptions.CommentDoesNotExistException;
import com.telerikproject.models.Comment;
import com.telerikproject.models.Like;
import com.telerikproject.models.User;
import com.telerikproject.repositories.CommentRepository;
import com.telerikproject.services.interfaces.CommentService;
import com.telerikproject.services.interfaces.PostService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository;

    private UserService userService;

    private PostService postService;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, UserService userService, PostService postService) {
        this.commentRepository = commentRepository;
        this.userService = userService;
        this.postService = postService;
    }

    @Override
    public Comment createComment(CommentDTO commentDTO, User user) {
        Comment comment = new Comment();

        comment.setPost(postService.getPostById(commentDTO.getPostId()));
        comment.setUser(user);
        comment.setContent(commentDTO.getContent());
        comment.setCommentDate(LocalDateTime.now());

        return commentRepository.save(comment);
    }

    @Override
    public List<Comment> getAllComments() {
        List<Comment> comments = commentRepository.findAll();

        if (comments.isEmpty()) {
            throw new EntityNotFoundException();
        }

        return comments;
    }

    @Override
    public Comment getCommentById(Integer id) {
        Optional<Comment> comment = commentRepository.findById(id);

        if (!comment.isPresent()) {
            throw new CommentDoesNotExistException();
        }
        return comment.get();
    }

    @Override
    public Comment editComment(Integer id, Comment comment) {
        Comment commentToUpdate = getCommentById(id);
        if (commentToUpdate == null) {
            throw new EntityNotFoundException();
        }

        commentToUpdate.setContent(comment.getContent());
        commentToUpdate.setCommentDate(LocalDateTime.now());
        return commentRepository.save(commentToUpdate);
    }

    @Transactional
    @Override
    public void deleteComment(Integer id) {
        Comment comment = getCommentById(id);

        if (comment == null) {
            throw new CommentDoesNotExistException();
        }

        commentRepository.deleteAllById(id);
    }

    @Override
    @Transactional
    public void deleteCommentsByPostId(Integer id) {
        commentRepository.deleteAllCommentsByPostId(id);
    }
}
