package com.telerikproject.services;

import com.telerikproject.enums.ConnectionStatus;
import com.telerikproject.exceptions.ExistingException;
import com.telerikproject.models.Connection;
import com.telerikproject.models.User;
import com.telerikproject.repositories.ConnectionRepository;
import com.telerikproject.repositories.UserRepository;
import com.telerikproject.services.interfaces.ConnectionService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ConnectionServiceImpl implements ConnectionService {

    private ConnectionRepository connectionRepository;
    private UserRepository userRepository;
    private UserService userService;

    @Autowired
    public ConnectionServiceImpl(ConnectionRepository connectionRepository, UserRepository userRepository, UserService userService) {
        this.connectionRepository = connectionRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @Override
    public List<User> getAllFriends(User user) {
        List<Connection> connections = connectionRepository.getAllBySecondUserAndStatus(user, ConnectionStatus.ACCEPTED);
        if (connections == null) {
            throw new EntityNotFoundException();
        }
        List<User> users = new ArrayList<>();

        connections.forEach(relationship -> users.add(relationship.getFirstUser()));

        return users;
    }

    @Override
    public List<Connection> getAllSendRequests(User user) {
        return connectionRepository.getAllByFirstUserAndStatus(user, ConnectionStatus.PENDING);
    }

    @Override
    public void sendFriendRequest(String name, User user) {
        User userReceiveRequest = getUser(name);

        Connection currentConnection = connectionRepository.
                findByFirstUserAndSecondUserAndStatus(user, userReceiveRequest, ConnectionStatus.PENDING);

        Connection currentConnectionFromOtherPerson = connectionRepository.
                findByFirstUserAndSecondUserAndStatus(user, userReceiveRequest, ConnectionStatus.PENDING);

        if (currentConnection != null || currentConnectionFromOtherPerson != null) {
            throw new ExistingException();
        }

        Connection newRelationship =
                new Connection(user, userReceiveRequest, ConnectionStatus.PENDING);

        connectionRepository.saveAndFlush(newRelationship);
    }

    @Override
    public void declineFriendRequest(String name, User user) {
        User userDeclineFriendRequest = getUser(name);

        Connection currentConnection = getConnection(user, userDeclineFriendRequest);

        currentConnection.setStatus(ConnectionStatus.DECLINED);

        connectionRepository.saveAndFlush(currentConnection);
    }

    @Override
    public void deleteFriendRequest(String name, User userDeleteRequest) {
        User loginUser = userService.getUserByUsername(name);

        Connection connection = getConnection(loginUser, userDeleteRequest);

        connectionRepository.delete(connection);
    }


    @Override
    public User acceptFriendRequest(String name, User user) {
        User userAcceptFriendRequest = getUser(name);

        Connection currentConnection = getConnection(user, userAcceptFriendRequest);

        currentConnection.setStatus(ConnectionStatus.ACCEPTED);

        connectionRepository.saveAndFlush(currentConnection);

        return userAcceptFriendRequest;
    }

    @Override
    public List<User> getAllRequestsFromUsers() {
        List<Connection> connections = connectionRepository.
                getAllBySecondUserAndStatus(getUser(userService.findLoginUserName()), ConnectionStatus.PENDING);

        if (connections == null) {
            throw new EntityNotFoundException();
        }
        List<User> users = new ArrayList<>();

        connections.forEach(relationship -> users.add(relationship.getFirstUser()));

        return users;
    }

    @Override
    public List<User> getAllFriendsByUser(User user) {
        List<Connection> connections = connectionRepository.
                getAllBySecondUserAndStatus(getUser(userService.findLoginUserName()), ConnectionStatus.ACCEPTED);

        if (connections == null) {
            throw new EntityNotFoundException();
        }
        List<User> users = new ArrayList<>();

        connections.forEach(relationship -> users.add(relationship.getFirstUser()));

        return users;
    }

    private Connection getConnection(User loginUser, User otherUser) {
        Connection currentConnection = connectionRepository.
                findByFirstUserAndSecondUser(loginUser, otherUser);

        if (currentConnection == null) {
            Connection currentConnectionReversed = connectionRepository.
                    findByFirstUserAndSecondUser(otherUser, loginUser);
            if (currentConnectionReversed != null) {
                return currentConnectionReversed;
            } else {
                throw new EntityNotFoundException();
            }
        }
        return currentConnection;
    }

    public User getUser(String name) {
        User userDeclineFriendRequest = userRepository.getUserByUsername(name);

        if (userDeclineFriendRequest == null) {
            throw new EntityNotFoundException();
        }
        return userDeclineFriendRequest;
    }
}
