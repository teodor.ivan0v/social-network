package com.telerikproject.services;

import com.telerikproject.DTOs.PostDTO;
import com.telerikproject.exceptions.PostDoesNotExistException;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import com.telerikproject.repositories.CommentRepository;
import com.telerikproject.repositories.PostRepository;
import com.telerikproject.services.interfaces.PostService;
import com.telerikproject.services.interfaces.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    private PostRepository postRepository;
    private CommentRepository commentRepository;
    private ModelMapper modelMapper;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, CommentRepository commentRepository, ModelMapper modelMapper) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Post createPost(PostDTO postDTO, User user) {
        Post post = modelMapper.map(postDTO, Post.class);
        post.setUser(user);
        post.setPostDate(LocalDateTime.now());
        return postRepository.save(post);
    }

    @Override
    public Post getPostById(Integer id) {
        Optional<Post> post = postRepository.findById(id);

        if (!post.isPresent()) {
            throw new PostDoesNotExistException();
        }
        return post.get();
    }

    @Override
    public List<Post> getAll() {
        return postRepository.findAll();
    }

    @Override
    public List<Post> getAllPublicPostsOrderByPostDateDesc() {
        return postRepository.getAllPublicPostsOrderByPostDateDesc();
    }

    @Override
    public List<Post> getAllUserPostsById(Integer id) {
        return postRepository.getAllByUserId(id);
    }

    @Override
    public List<Post> getAllCurrentUserPosts(User user) {
        return postRepository.getAllPostsOrderByPostDateDesc()
                .stream()
                .filter(post -> post.getUser().getId() == user.getId())
                .collect(Collectors.toList());
    }

    @Override
    public Post editPost(Integer id, Post post) {
        Post postToBeUpdated = getPostById(id);

        if (postToBeUpdated == null) {
            throw new EntityNotFoundException();
        }

        postToBeUpdated.setContent(post.getContent());
        postToBeUpdated.setStatus(post.isStatus());
        postToBeUpdated.setPostDate(post.getPostDate());

        return postRepository.save(postToBeUpdated);
    }

    @Override
    public void deletePost(Integer id) {
        Post post = getPostById(id);

        if (post == null) {
            throw new PostDoesNotExistException();
        }

        commentRepository.deleteAllCommentsByPostId(id);
        postRepository.delete(post);
    }
}
