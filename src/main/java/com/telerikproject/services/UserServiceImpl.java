package com.telerikproject.services;

import com.telerikproject.exceptions.UserDoesNotExistException;
import com.telerikproject.models.Authority;
import com.telerikproject.models.Connection;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import com.telerikproject.repositories.ConnectionRepository;
import com.telerikproject.repositories.UserRepository;
import com.telerikproject.services.interfaces.AuthorityService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private AuthorityService authorityService;
    private ConnectionRepository connectionRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, AuthorityService authorityService, ConnectionRepository connectionRepository) {
        this.userRepository = userRepository;
        this.authorityService = authorityService;
        this.connectionRepository = connectionRepository;
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getCurrentUser(Principal principal) {
        Optional<User> user = userRepository.findByUsername(principal.getName());

        if(!user.isPresent()) {
            throw new EntityNotFoundException();
        }
        return user.get();
    }

    @Override
    public List<Post> getUserPosts(int id) {
        if (!userExists(id)) {
            throw new UserDoesNotExistException();
        }
        User user = getUserById(id);
        return user.getPosts();
    }

    @Override
    public User getUserById(int id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent()) {
            throw new UserDoesNotExistException();
        }
        return user.get();
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    @Override
    public void deleteUser(String username) {
        User user = getUserByUsername(username);
        Authority authority = authorityService.getAuthorityByUsername(username);

        if (user == null) {
            throw new EntityNotFoundException();
        }

        List<Connection> connections = connectionRepository.getAllByFirstUser(user);

        authorityService.deleteAuthority(username);

        for (Connection connection : connections) {
            connectionRepository.delete(connection);
        }



        userRepository.delete(user);
    }

    @Override
    public String findLoginUserName() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @Override
    public User save(Integer id, User user){
        User userToBeUpdated = getUserById(id);

        if (userToBeUpdated == null) {
            throw new EntityNotFoundException();
        }

        userToBeUpdated.setFirstName(user.getFirstName());
        userToBeUpdated.setLastName(user.getLastName());
        userToBeUpdated.setLastName(user.getLastName());
        userToBeUpdated.getUserDetails().setCity(user.getUserDetails().getCity());
        userToBeUpdated.getUserDetails().setCountry(user.getUserDetails().getCountry());
        userToBeUpdated.getUserDetails().setAbout(user.getUserDetails().getAbout());

        return userRepository.save(userToBeUpdated);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    public boolean userExists(Integer id) {
        return userRepository.existsById(id);
    }


}
