package com.telerikproject.services.interfaces;

import com.telerikproject.DTOs.CommentDTO;
import com.telerikproject.models.Comment;
import com.telerikproject.models.User;

import java.security.Principal;
import java.util.List;

public interface CommentService {

    Comment createComment(CommentDTO commentDTO, User user);

    List<Comment> getAllComments();

    Comment getCommentById(Integer id);

    void deleteComment(Integer id);

    void deleteCommentsByPostId(Integer id);

    Comment editComment(Integer id, Comment comment);
}
