package com.telerikproject.services.interfaces;

import com.telerikproject.models.Like;
import com.telerikproject.models.User;

import java.security.Principal;
import java.util.List;

public interface LikeService {

    List<Like> getAllLikes();

    List<Like> getAllLikesByPostId(Integer id);

    Like getLikeById(Integer id);

    void deleteLikeById(Integer id);

    void likeAndDislikePost(Integer postId, User user);

}
