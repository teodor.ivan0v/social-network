package com.telerikproject.services.interfaces;


import com.telerikproject.models.*;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

public interface UserService {

    User createUser(User user) throws IOException;

    List<User> getAllUsers();

    User getCurrentUser(Principal principal);

    List<Post> getUserPosts(int id);

    User getUserById(int id);

    User getUserByUsername(String username);

    void deleteUser(String username);

    String findLoginUserName();

    User save(Integer id, User user);

    void save(User user);

}
