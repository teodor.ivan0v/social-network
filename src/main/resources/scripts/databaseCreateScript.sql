use socialnetwork;

create table user_details
(
    id              bigint auto_increment
        primary key,
    image_path varchar(255) null,
    city       varchar(50)  null,
    country    varchar(50)  null,
    about      text         null
);

create table users
(
    id              bigint auto_increment
        primary key,
    first_name      varchar(50)  null,
    last_name       varchar(50)  null,
    username        varchar(50)  null,
    password        varchar(255) null,
    gender          varchar(50)  null,
    enabled         tinyint(1)   null,
    user_details_id bigint          null,
    constraint users_username_uindex
        unique (username),
    constraint users_user_details_id_fk
        foreign key (user_details_id) references user_details (id)
);

create table authorities
(
    authority varchar(50) null,
    username  varchar(50) null,
    constraint authorities_users_username_fk
        foreign key (username) references users (username)
);

create table connections
(
    id             bigint auto_increment
        primary key,
    first_user_id  bigint null,
    second_user_id bigint null,
    status         int    null,
    constraint connections_users_id_fk
        foreign key (first_user_id) references socialnetwork.users (id),
    constraint connections_users_id_fk_2
        foreign key (second_user_id) references socialnetwork.users (id)
);

create table posts
(
    id        bigint auto_increment
        primary key,
    content   text       null,
    is_public tinyint(1) null,
    post_date datetime   null,
    user_id   bigint        null,
    constraint posts_users_id_fk
        foreign key (user_id) references users (id)
);

create table comments
(
    id           bigint auto_increment
        primary key,
    content      text     null,
    comment_date datetime null,
    user_id      bigint      null,
    post_id      bigint      null,
    constraint comment_user_fk
        foreign key (user_id) references users (id),
    constraint comments_posts_id_fk
        foreign key (post_id) references posts (id)
);

create table likes
(
    id      bigint auto_increment
        primary key,
    user_id bigint null,
    post_id bigint null,
    constraint likes_posts_id_fk
        foreign key (post_id) references posts (id),
    constraint likes_users_id_fk
        foreign key (user_id) references users (id)
);

