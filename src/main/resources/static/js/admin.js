$(document).ready(function () {
    const usersUrl = "http://localhost:8080/api/admin/all/users";
    const postsUrl = "http://localhost:8080/api/posts/all";
    const commentUrl = "http://localhost:8080/comments/all";

    $.ajax({
        url: postsUrl,
        type: "GET",
        async: true,
        success: function (data) {
            getPosts(data)
        },
        error: function () {
            console.log('error in displaying all posts')
        }
    });

    function getPosts(posts) {
        let allPosts = Object.values(posts);
        console.log(posts);

        for (let i = 0; i < allPosts.length; i++) {
            let postObj;


            postObj =
                `<tr>
                <td>${allPosts[i].id}</td>
                    <td>${allPosts[i].user.id}</td>
                    <td>${allPosts[i].postDate}</td>
                    <td>
                    <a id="editPost${allPosts[i].id}" href="/admin/edit/post/${allPosts[i].id}"
            class="btn btn-primary">
                    <i class="fas fa-user-edit ml-2"></i>
                    </a>
                    </td>
                    <td>
                    <button onclick='deletePost(${allPosts[i].id}); window.location.reload()' type="button" id="deletePost${allPosts[i].id}" class="btn btn-primary">
                    <i class="fas fa-user-times ml-2"></i>
                    </button>
                    </td>
                    </tr>`;
            $("#posts").append(postObj);

            let commentObj;

            for (let j = 0; j < posts[i].comments.length; j++) {
                commentObj = `
                <tr>
                    <td>${allPosts[i].comments[j].id}</td>
                    <td>${allPosts[i].id}</td>
                    <td>${allPosts[i].comments[j].user.username}</td>
                    <td>
                    <a id="editPost${allPosts[i].comments[j].id}" href="/admin/edit/comment/${allPosts[i].comments[j].id}"
            class="btn btn-primary">
                    <i class="fas fa-user-edit ml-2"></i>
                    </a>
                    </td>
                    <td>
                    <button onclick='deleteComment(${allPosts[i].comments[j].id}); window.location.reload()' type="button" id="deletePost${allPosts[i].comments[j].id}" class="btn btn-primary">
                    <i class="fas fa-user-times ml-2"></i>
                    </button>
                    </td>
                    </tr>`;

                $("#comments").append(commentObj);
            }
        }
        }



    $.ajax({
        url: usersUrl,
        type: "GET",
        async: true,
        success: function (data) {
            getUsers(data)
        },
        error: function () {
            console.log('error in displaying all users')
        }
    });


    function getUsers(users) {
        let allUsers = Object.values(users);

        console.log(allUsers);

        for (let i = 0; i < users.length; i++) {
            let userObj;


            userObj =
                `<tr>
                <td>${users[i].firstName}</td>
                    <td>${users[i].lastName}</td>
                    <td>${users[i].username}</td>
                    <td>
                    <a id="editUser${users[i].id}" href="/admin/edit/${users[i].id}"
            class="btn btn-primary">
                    <i class="fas fa-user-edit ml-2"></i>
                    </a>
                    </td>
                    <td>
                    <button onclick='deleteUser("${users[i].username}"); window.location.reload()' type="button" id="deleteUser${users[i].id}" class="btn btn-primary">
                    <i class="fas fa-user-times ml-2"></i>
                    </input>
                    </td>
                    </tr>`;
            $("#users").append(userObj);

            // let myId = users[i].id,
            //     editUserElement = document.getElementById('editUser' + myId),
            //     editPostElement = document.getElementById('editPost' + myId);
            //
            //
            // debugger
            // editUserElement.href += myId;
            // editPostElement.href += myId;


        }
    }
});

function deletePost(item) {
    debugger
    return fetch("/admin/delete/post/" + item, {
        method: 'delete'
    })
}

function deleteUser(item) {
    debugger
    return fetch("/admin/delete/user/" + item, {
        method: 'delete'
    })
}

function deleteComment(item) {
    debugger
    return fetch("/admin/delete/comment/" + item, {
        method: 'delete'
    })
}

function editUser(item) {
    debugger
    return fetch("/admin/edit/user/" + item, {
        method: 'put'
    })
}

function editPost(item) {
    debugger
    return fetch("/admin/edit/post/" + item, {
        method: 'put'
    })
}

function editComment(item) {
debugger
    return fetch("/admin/edit/comment/" + item, {
        method: 'put'
    })
}

