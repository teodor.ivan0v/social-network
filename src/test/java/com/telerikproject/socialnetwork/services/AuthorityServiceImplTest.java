package com.telerikproject.socialnetwork.services;

import com.telerikproject.repositories.AuthorityRepository;
import com.telerikproject.services.AuthorityServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthorityServiceImplTest {

    @Mock
    AuthorityRepository authorityRepository;

    @InjectMocks
    AuthorityServiceImpl authorityService;

    @Test
    public void exist_Should_CallRepositoryExist_When_UserExist() {
        // Act
        authorityService.doesAuthorityExist("name");

        // Assert
        Mockito.verify(authorityRepository, Mockito.times(1)).existsById("name");
    }

//    @Test
//    public void delete_Should_CallRepositoryDelete_When_DeletingLike() {
//        String id = "name";
//        Authority authority = new Authority("name", "ROLE_USER");
//
//        when(authorityRepository.findById(id))
//                .thenReturn(java.util.Optional.of(authority));
//
//        authorityService.deleteAuthority(id);
//    }

//    public void create_Should_CallRepository_When_Create_NewUser() {
//        // Arrange
//        Authority authority = new Authority("name", "ROLE_USER");
//
//        // Act
//        authorityService.addNewAuthority(authority);
//
//        // Assert
//        Mockito.verify(authorityRepository, Mockito.times(0))
//                .save(authority);
//    }
}