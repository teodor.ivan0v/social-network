package com.telerikproject.socialnetwork.services;

import com.telerikproject.enums.ConnectionStatus;
import com.telerikproject.models.Connection;
import com.telerikproject.models.User;
import com.telerikproject.repositories.ConnectionRepository;
import com.telerikproject.repositories.UserRepository;
import com.telerikproject.services.ConnectionServiceImpl;
import com.telerikproject.services.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConnectionServiceImplTest {

    @Mock
    private ConnectionRepository connectionRepository;
    @Mock
    private UserRepository userRepository;

    private ConnectionServiceImpl connectionService;

    private UserServiceImpl userService;

    private List<Connection> connections;


    @Before
    public void setUp() {

        connections = new ArrayList<>();
        User user = new User("username", "password");
        connections.add(new Connection( user, user, ConnectionStatus.ACCEPTED));
        connections.add(new Connection( user, user, ConnectionStatus.ACCEPTED));
        connections.add(new Connection( user, user, ConnectionStatus.ACCEPTED));
        connections.add(new Connection( user, user, ConnectionStatus.ACCEPTED));
        connectionService = new ConnectionServiceImpl(connectionRepository, userRepository, userService);
    }

    @Test
    public void testGetAllFriends() {
        // Setup

        final String username = "username";
        User user = new User("username", "password");

        when(connectionRepository.getAllBySecondUserAndStatus(user, ConnectionStatus.ACCEPTED)).thenReturn(connections);
        when(connectionRepository.getAllByFirstUserAndStatus(user, ConnectionStatus.ACCEPTED)).thenReturn(connections);

        when(userRepository.getUserByUsername(username)).thenReturn(user);
        when(connectionService.getUser(username)).thenReturn(user);

        // Run the test
        List<User> result = connectionService.getAllFriends(user);

        // Verify the results
        assertEquals(4, result.size());
    }

    @Test
    public void testSendFriendRequestFromOtherPerson() {
        String username = "username";
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(authentication.getName()).thenReturn("username");

        User user = new User("username", "password");


        when(userRepository.getUserByUsername(username)).thenReturn(user);
        when(connectionService.getUser(username)).thenReturn(user);

        // Run the test
        connectionService.sendFriendRequest(username, user);
    }


    @Test(expected = EntityNotFoundException.class)
    public void testDeclineFriendRequestShouldThrowException() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(authentication.getName()).thenReturn("username");
        final String username = "username";
        User user = new User("username", "password");

        when(userRepository.getUserByUsername(username)).thenReturn(user);
        when(connectionService.getUser(username)).thenReturn(user);

        when(userRepository.getUserByUsername(username)).thenReturn(user);

        connectionService.declineFriendRequest(username, user);

    }

}
