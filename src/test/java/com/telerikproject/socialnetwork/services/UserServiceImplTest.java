package com.telerikproject.socialnetwork.services;

import com.telerikproject.exceptions.UserDoesNotExistException;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import com.telerikproject.repositories.UserRepository;
import com.telerikproject.services.UserServiceImpl;
import com.telerikproject.services.interfaces.AuthorityService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    UserRepository userRepository;

    @Mock
    private Principal principal;

    @Mock
    private AuthorityService authorityService;

    @InjectMocks
    UserServiceImpl userService;

    @Before
    public void setUp() {

    }

    @Test
    public void create_Should_CallRepository_When_Create_NewUser() {
        // Arrange
        User user = new User("TestFirstName1", "TestLastName1");

        // Act
        userService.createUser(user);

        // Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .save(user);
    }

    @Test
    public void getAllUsers_Should_ReturnAllUsers() {
        // Arrange
        Mockito.when(userService.getAllUsers()).thenReturn(Arrays.asList(
                new User("username1", "password1"),
                new User("username2", "password2"),
                new User("username3", "password3")
        ));

        // Act
        List<User> result = userService.getAllUsers();

        // Assert
        assertEquals(3, result.size());
    }

    @Test
    public void getById_Should_ReturnUser_When_User_Exist() {
        // Arrange
        User user = new User("testUsername1", "12345");

        Mockito.when(userRepository.findById(1)).thenReturn(Optional.of(user));

        // Act
        User result = userService.getUserById(1);

        // Assert
        assertEquals(user, result);
    }

    @Test(expected = UserDoesNotExistException.class)
    public void getById_Should_ThrowException_When_User_NotExist() {
        // Arrange
        User user = new User("testUsername1", "12345");

        // Act
        userService.getUserById(2);
    }

    @Test
    public void update_Should_CallRepositoryUpdate_When_UpdatingUser() {
        // Arrange
        User user = new User("TestName1", "password1");

        // Act
        userService.save(user);

        // Assert
        Mockito.verify(userRepository, Mockito.times(1)).save(user);
    }

    @Test
    public void exist_Should_CallRepositoryExist_When_UserExist() {
        // Act
        userService.userExists(1);

        // Assert
        Mockito.verify(userRepository, Mockito.times(1)).existsById(1);
    }

    @Test(expected = EntityNotFoundException.class)
    public void delete_Should_ThrowEntityNotFoundException_When_DeletingUser() {
        // Arrange
        User user = new User("TestName1", "password1");

        // Act
        userService.deleteUser(user.getUsername());
    }

//    @Test
//    public void delete_Should_CallRepositoryDelete_When_DeletingUser() {
//        // Arrange
//        User user = new User("TestName1", "password1");
//
//        // Act
//        Mockito.when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.of(user));
//        userService.deleteUser(user.getUsername());
//
//        // Assert
//        Mockito.verify(userRepository, Mockito.times(1)).delete(user);
//    }

//    @Test
//    publi  c void searchByUsername_Should_CallRepositorySearch_When_SearchingUser() {
//        // Arrange
//        User user1 = new User("Test@Name1", "password1");
//        User user2 = new User("Test@Name1", "password2");
//        List<User> users = new ArrayList<User>() {
//            {
//                add(user1);
//                add(user2);
//            }
//        };
//
//        // Act
//        Mockito.when(userRepository.searchByUsername(user1.getUsername())).thenReturn(users);
//        List<User> foundUsers = userService.userSearch(user1.getUsername());
//        // Assert
//        assertEquals(2, foundUsers.size());
//    }

    @Test(expected = UserDoesNotExistException.class)
    public void getUserPosts_Should_ReturnPosts_Expected_throwException() {
        //Act
        userService.getUserPosts(1);

    }

    @Test
    public void getUserPosts_Should_ReturnPosts() {
        // Arrange
        User user = new User("TestName1", "password1");
        List<Post> posts = new ArrayList<Post>(){
            {
                add(new Post());
                add(new Post());
                add(new Post());
            }
        };

        user.setPosts(posts);

        // Act
        Mockito.when(userRepository.existsById(1)).thenReturn(true);
        Mockito.when(userRepository.findById(1)).thenReturn(Optional.of(user));
        List<Post> userPosts = userService.getUserPosts(1);

        // Assert
        assertEquals(3, userPosts.size());
    }

    //TODO
    //    @Test
//    public void getCurrentUser_Should_CallRepositoryUpdate_When_getCurrentUser() {
//        // Arrange
//        User user = new User("TestName1", "password1");
//
//        // Act
//        Mockito.when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.of(user));
//        User user1 = userService.getCurrentUser(principal);
//        // Assert
//        assertEquals(user1, user);
//    }


//    @Test
//    public void testGetCurrentUser() {
//        // Setup
//        final Principal principal = null;
//        final User expectedResult = new User();
//        when(userRepository.findByUsername("email")).thenReturn(Optional.empty());
//
//        // Run the test
//        final User result = userService.getCurrentUser(principal);
//
//        // Verify the results
//        assertEquals(expectedResult, result);
//    }
}
